# -*- coding: utf-8 -*-
##########################################################################
# OpenLP - Open Source Lyrics Projection                                 #
# ---------------------------------------------------------------------- #
# Copyright (c) 2008-2021 OpenLP Developers                              #
# ---------------------------------------------------------------------- #
# This program is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with this program.  If not, see <https://www.gnu.org/licenses/>. #
##########################################################################
from test_api.apitest.logger import print_text, print_error, print_ok
from test_api.apitest.zero import check_for_openlp
from test_api.apitest.runner import RunTestsController


def start() -> None:
    """
    Instantiate and run the tests.
    :return:
    """
    print_text('OpenLP - API Test Runner V0_3')
    print_text('Check OpenLP is running')
    op_address, op_http_port = check_for_openlp('http')
    if not op_http_port:
        print_error('OpenLP is not found - defaulting to 4316')
        op_http_port = 4316
    else:
        print_ok(f'OpenLP is running on port (http) {op_http_port}')
    _, op_ws_port = check_for_openlp('ws')
    if not op_ws_port:
        print_error('OpenLP is not found - defaulting to 4317')
        op_ws_port = 4317
    print_ok(f'OpenLP is running network Address {op_address}')
    print_ok(f'OpenLP is running on port (ws) {op_ws_port}')
    rtc = RunTestsController(op_address, op_http_port, op_ws_port)
    rtc.connect()
    rtc.read_command_file()
    print_ok('Finished running tests')


if __name__ == '__main__':
    start()
