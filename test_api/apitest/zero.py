# -*- coding: utf-8 -*-
##########################################################################
# OpenLP - Open Source Lyrics Projection                                 #
# ---------------------------------------------------------------------- #
# Copyright (c) 2008-2021 OpenLP Developers                              #
# ---------------------------------------------------------------------- #
# This program is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with this program.  If not, see <https://www.gnu.org/licenses/>. #
##########################################################################
import socket
from zeroconf import ServiceBrowser, Zeroconf, ServiceInfo
from time import sleep

from test_api.apitest.logger import print_info


def check_for_openlp(type: str) -> str:
    class MyListener:
        def __init__(self):
            self.port = None
            self.address = "127.0.0.1"

        @staticmethod
        def remove_service(zeroconf: Zeroconf, type: str, name: str):
            print_info(f'Service {name} removed for {type}')

        def add_service(self, zeroconf: Zeroconf, type: str, name: str):
            info = zeroconf.get_service_info(type, name)
            print_info(f'Service {name} added, service info: {info}')
            if info.name.startswith("OpenLP"):
                print_info(f'Service {name} found')
                self.port = str(info.port)
                self.address = socket.inet_ntoa(info.addresses[0])

        @staticmethod
        def update_service(info: ServiceInfo) -> None:
            pass

    zeroconf = Zeroconf()
    my_listener = MyListener()
    _ = ServiceBrowser(zeroconf, f'_{type}._tcp.local.', my_listener)
    sleep(0.1)
    zeroconf.close()
    return my_listener.address, my_listener.port
