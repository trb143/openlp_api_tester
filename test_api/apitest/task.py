# -*- coding: utf-8 -*-
##########################################################################
# OpenLP - Open Source Lyrics Projection                                 #
# ---------------------------------------------------------------------- #
# Copyright (c) 2008-2021 OpenLP Developers                              #
# ---------------------------------------------------------------------- #
# This program is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with this program.  If not, see <https://www.gnu.org/licenses/>. #
##########################################################################
import importlib
import time
from test_api.apitest.callbacks import *  # noqa E403


def human_delay(delay: int = 2) -> None:
    """
    Delay time in Seconds
    :param delay:
    :return:
    """
    time.sleep(delay)


class Task(object):

    def __init__(self, caller: object, request: dict) -> None:
        self.caller = caller
        self.request = request
        callback = self.request['name']
        function_string = f'test_api.apitest.callbacks.{callback}'
        mod_name, func_name = function_string.rsplit('.', 1)
        mod = importlib.import_module(mod_name)
        func = getattr(mod, func_name)
        self.__setattr__("callback", func)

    def run(self):
        try:
            payload = self.request['payload']
            self.callback(self.caller, payload)
        except KeyError:
            self.callback(self.caller)
        try:
            human_delay(self.request['delay'])
        except KeyError:
            human_delay(0.3)
        try:
            self.caller.check_websocket_changes(self.request['max'], self.request['min'])
        except KeyError:
            pass
