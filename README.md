# OpenLP API tester.

A command line utility to access a running OpenLP instance via it's API's and test all functionality.


This harness will find a running OpenLP instance on the network via ZeroConf and then using the API's build and run a service.
After each command the results are tested to make sure the WebSockets have changed. 

The API's need to be configured as insecure as this is not being tested.

Tests are mastered in JSON files and run using the framework.

The bible test framework is random so some bible references will not be correct and therefore will not load!  The chapter and verse numbers are random numbers!!

## Sample Test Structure
```
process_name: test process
step1:
  max: 1
  min: 1
  name: clear_live_controller
step2:
  delay: 1
  name: clear_preview_controller
step3:
  max: 1
  min: 1
  name: new_service
step4:
  delay: 1
  name: search_and_add
  payload:
    plugin: songs
step6:
  name: load_service_sequential 
```
* process_name

   A unique name for the run
   
* step 1

   The step number, needs to be unique and in order!
   
   * delay (optional)
   
      How long to wait after a call has been made.  Default is 0.3 seconds for all calls.

  * max (optional)
   
      Max number of fields which will change on the Web Sockets interface.

  * min (optional)
   
      Max number of fields which will change on the Web Sockets interface.

  * payload
   
     Json to support addition information to the call.

  * name
   
      The step name (function name) to be called to run the step.


## Commands and Payloads
| Rule Name                | Payload                      |
|--------------------------|------------------------------|
| trigger_alert            | text: <Some text to display> |
| search_and_add | plugin: <plugin name>|
| search_and_live | plugin: <plugin name>|
| service_item_show | item: item_id|
| play_live_item | ?????|
| controller_item_next |?????|
| controller_item_previous |???????|


## Adding API's and Internals
API's and their handers are defining in the callback.py file.  This should be the only file that needs to change when adding new API's

The test scripts are in the scripts directory and are a pre defined set of tests.



